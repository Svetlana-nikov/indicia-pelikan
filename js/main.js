function updateSpinner(obj) {
    var contentObj = document.getElementById("content");
    var value = parseInt(contentObj.value);
    if (obj.id == "down") {
        value--;
    } else {
        value++;
    }
    contentObj.value = value + ' ' + 'pers.';
}


$(document).ready(function() {

    $(function() {
        $('.one-recepten').matchHeight();
        $('.item').matchHeight();


    });

    //Hide block info
    $('body').on('click', '.info-icon', function() {
        $(this).parent('div').find('.popover-box').toggle();
    });

    $('html').on('click', 'button.close', function() {
        $(this).parent('.popover-box').css('display', 'none');
    });

    // Sidebar check icons
    $('.sidebar-right-list ul li > div').click(function() {
        $(this).addClass('active');
    });

    $('.sidebar-recepten .plus-icon').click(function() {
        $(this).parent('div').addClass('active');
    });


    //Filter Box
    $(".filter-show a").click(function() {

        this.parentNode.classList.toggle('opened');
        if ($('.filter-show').hasClass("opened")) {
            var withFilterShow = document.getElementsByClassName("with-filter");
            var withoutFilterHide = document.getElementsByClassName("without-filter");
            $(withFilterShow).show();
            $(withoutFilterHide).hide();
        } else {
            var withFilterHide = document.getElementsByClassName("with-filter");
            var withoutFilterShow = document.getElementsByClassName("without-filter");
            $(withFilterHide).hide();
            $(withoutFilterShow).show();
        }
    });

    
    //Slick Slider
    $('.slider').slick({
        infinite: true,
        slidesToShow: 4,
        arrows: false,
        dots: true,
        slidesToScroll: 1,

        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true,
                    autoplay: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    autoplay: true,
                    dots: false,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    autoplay: true,
                    dots: false,
                    slidesToScroll: 1
                }
            }

        ]
    });



    //Login Form
    $(".login-form").hover(
        function() {
            $(this).addClass("hover");
        },
        function() {
            $(this).removeClass("hover");
        }
    );

    $("body").on("click", "a.dropdown-toggle", function() {
        setTimeout(function() {

            $('.menu-height').matchHeight();
            $('.list-box').matchHeight();

        }, 1000);

        $(window).resize(function() {
            $('.menu-height').matchHeight();
            $('.list-box').matchHeight();

        });
    });

    //Remove placeholder on click
    $("input,textarea").each(function() {
        $(this).data('holder', $(this).attr('placeholder'));

        $(this).focusin(function() {
            $(this).attr('placeholder', '');
        });

        $(this).focusout(function() {
            $(this).attr('placeholder', $(this).data('holder'));
        });
    });

});

equalheight = function(container) {

    var currentTallest = 0,
        currentRowStart = 0,
        rowDivs = [],
        $el,
        topPosition = 0;
    $(container).each(function() {

        $el = $(this);
        $($el).height('auto');
        topPostion = $el.position().top;

        if (currentRowStart != topPostion) {
            for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
            rowDivs.length = 0;
            currentRowStart = topPostion;
            currentTallest = $el.height();
            rowDivs.push($el);
        } else {
            rowDivs.push($el);
            currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
        }
        for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
            rowDivs[currentDiv].height(currentTallest);
        }
    });
};

$(window).load(function() {
    equalheight('.party-box');
    equalheight('.hot-box p');
    equalheight('.height-box');
    equalheight('.one-recepten');
    equalheight('.one-block');
    equalheight('.height-half');
    equalheight(".logo-one");
    equalheight(".list-boxes .list-box");
    equalheight(".one-actuele");

});

$(window).resize(function() {
    equalheight('.party-box');
    equalheight('.one-recepten');

   equalheight('.hot-box p');
    equalheight('.one-block');
    equalheight('.height-box');
    equalheight(".logo-one");
    equalheight(".list-boxes .list-box");
    equalheight(".one-actuele");
    $('.item').matchHeight();


});
//Custom Select
$("select").simpleselect();


$(".sidebar-recepten span i").click(function() {
    $(".sidebar-recepten").find('.popover-box').toggle();
});

$(".deel-recept").click(function() {
    $(this).toggleClass('active');
});

$(".top-search button").click(function() {
    $(this).parent('.top-search').toggleClass('active');

    if ($(event.target).closest('.top-search').length === 0) {
        $('.top-search').removeClass('active');
    }
});

$(".sidebar-recepten .close").click(function() {
    $(".popover-box").hide();
});

jQuery('form').on('submit', function(e){
    e.preventDefault();
});