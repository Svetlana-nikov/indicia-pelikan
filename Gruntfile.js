'use strict';

module.exports = function(grunt) {

  /* Configure
  ============================ */
  var configs = {

    css_combine_files: [
      'vendor/css/bootstrap.min.css',
      'vendor/css/slick.css',
      'vendor/css/jquery.simpleselect.css',
      'css/combined.css'
    ],

    js_combine_files: [
      'vendor/js/jquery-1.10.1.min.js',
      'vendor/js/modernizr-2.6.2-respond-1.1.0.min.js',
      'vendor/js/bootstrap.min.js',
      'vendor/js/slick.min.js',
      'vendor/js/picturefill.min.js',
      'vendor/js/jquery.matchHeight-min.js',
      'vendor/js/jquery.simpleselect.min.js',
      'js/main.js'
    ],

    js_hint_files: [
      'js/main.js'
    ],

    watch_files: [
      'less/*',
      'js/*',
      'vendor/css/*',
      'vendor/js/*'
    ],

    sync_watch_files: [
      'js/*',
      'vendor/css/*',
      'vendor/js/*',
      'css/combined.css',
      '*.html'
    ]
  }

  /* Init
  ============================ */
  grunt.initConfig({
    less: {
      production: {
        files: {
          "css/combined.css": "less/main.less"
        }
      }
    },
    jshint: {
      beforeconcat: configs.js_hint_files
    },
    concat: {
      options: {
        separator: ';',
      },
      dist: {
        src: configs.js_combine_files,
        dest: 'js/compiled.js',
      },
    },
    uglify: {
      my_target: {
        files: {
          'js/compiled.min.js': 'js/compiled.js'
        }
      }
    },
    browserSync: {
      bsFiles: {
        src: configs.sync_watch_files
      },
      options: {
        watchTask: true,
        server: {
          baseDir: "./"
        }
      }
    },
    cssmin: {
      combine: {
        files: {
          'css/main.min.css': configs.css_combine_files
        }
      }
    },
    watch: {
      src: {
        files: 'less/*',
        tasks: ['less']
      }
    }

  });

  // Add plugins
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-browser-sync');

  // Register tasks
  grunt.registerTask('build', ['less', 'cssmin', 'concat', 'uglify', 'jshint']);
  grunt.registerTask('default', ['browserSync', 'watch', 'jshint']);

  grunt.event.on('watch', function(action, filepath) {
    grunt.log.writeln(filepath + ' has ' + action);
  });

};